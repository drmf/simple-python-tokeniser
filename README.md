Simple ELG-compliant Python app
===============================

This is a very simple example of one way to create an app in Python that exposes an ELG-compliant endpoint, and an example infrastructure for packaging that app into a Docker container.  It aims to showcase some best-practice techniques, in particular:

- The image this example builds is derived `FROM` the Alpine base image for Python 3, which is significantly more compact than the standard `python:3` base image (90MB vs over 900MB)
- We create and switch to an unprivileged user account inside the container, so the Python code is not running as root.
- The image uses `tini` so that the Python process is not running as PID 1 inside the container.
