#!/usr/bin/env python3
#
#    Copyright 2020 European Language Grid
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#
# Very simple-minded whitespace tokeniser that returns a Token annotation for
# every sequence of non-whitespace characters in the supplied text.
#

from flask import Flask, request
from flask_json import FlaskJSON, JsonError, json_response, as_json
import re

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)

# Pre-compile the regular expression
non_space_pattern = re.compile(r'\S+')

@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={ 'errors': [
        { 'code':'elg.request.invalid', 'text':'Invalid request message' }
    ] })

def token_annotation(match):
    """Converts a re.Match object into an ELG-compliant annotation"""
    return {
      'start':match.start(), 'end':match.end(),
      'features':{'string':match.group()}
    }

@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if (data.get('type') != 'text') or ('content' not in data):
        invalid_request_error(None)

    content = data['content']
    tokens = [token_annotation(m) for m in non_space_pattern.finditer(content)]

    return dict(response = { 'type':'annotations', 'annotations':{'Token':tokens} })

if __name__ == '__main__':
    app.run()
